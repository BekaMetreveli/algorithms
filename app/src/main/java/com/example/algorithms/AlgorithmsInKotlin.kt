package com.example.algorithms

import java.util.*
import kotlin.collections.HashSet
import kotlin.math.abs
import kotlin.math.max

class AlgorithmsInKotlin {

    //Updated
    //Binary Gap
    fun solutionBinaryGap(N: Int): Int {
        var decimal = N

        var binaryGap = 0
        var tempGap = 0

        var isOne = false
        while (decimal != 0) {
            if (decimal % 2 == 0) {
                if (isOne) tempGap++
            } else {
                isOne = true
                if (tempGap > binaryGap) {
                    binaryGap = tempGap
                }
                tempGap = 0
            }
            decimal /= 2
        }
        return binaryGap
    }

    //Updated
    //MaxProductOfThree
    fun solutionMaxProductOfThree(A: IntArray): Int {
        if (A.size == 3) {
            return A[0] * A[1] * A[2]
        }
        var firstSmallest: Int
        var secondSmallest: Int

        var firstLargest: Int
        var secondLargest: Int
        var thirdLargest: Int

        firstSmallest = 1000
        secondSmallest = 1000

        firstLargest = -1000
        secondLargest = -1000
        thirdLargest = -1000
        for (i in A.indices) {
            when {
                A[i] > firstLargest -> {
                    thirdLargest = secondLargest
                    secondLargest = firstLargest
                    firstLargest = A[i]
                }
                A[i] > secondLargest -> {
                    thirdLargest = secondLargest
                    secondLargest = A[i]
                }
                A[i] > thirdLargest -> {
                    thirdLargest = A[i]
                }
            }

            if (A[i] < firstSmallest) {
                secondSmallest = firstSmallest
                firstSmallest = A[i]
            } else if (A[i] < secondSmallest) {
                secondSmallest = A[i]
            }
        }

        val max1 = firstLargest * secondLargest * thirdLargest
        val max2 = firstSmallest * secondSmallest * firstLargest
        return max(max1, max2)
    }

    //CyclicRotation
    fun solution(A: IntArray, K: Int): IntArray {
        val B = IntArray(A.size)

        if (A.size == 1) return A
        if (K == A.size) return A

        for (i in A.indices) {
            B[((i + K) % A.size)] = A[i]
        }
        return B
    }

    //PermMissingElement
    fun solutionMissingElement(A: IntArray): Int {
        val B = IntArray(A.size + 1)
        for (i in B.indices) {
            B[i] = -1
        }
        for (i in A.indices) {
            B[A[i] - 1] = A[i]
        }
        for (i in B.indices) {
            if (B[i] == -1) {
                return i + 1
            }

        }
        return -1
    }

    //FrogJumps
    fun solution(X: Int, Y: Int, D: Int): Int {
        return if ((Y - X) % D == 0) ((Y - X) / D)
        else ((Y - X) / D) + 1
    }

    //Brackets
    fun solutionBrackets(S: String): Int {
        if (S.length % 2 != 0) return 0

        val openingBrace = '{'
        val openingBracket = '['
        val openingParenthesis = '('

        val stack = Stack<Char>()

        for (i in S.indices) {
            val char: Char = S[i]
            if (char == openingBrace || char == openingBracket || char == openingParenthesis) {
                stack.push(char)
            } else {
                if (stack.isEmpty()) return 0
                if (i == S.length - 1 && stack.size != 1) return 0

                val openingChar = stack.pop()

                when (char) {
                    '}' -> if (openingChar != openingBrace) return 0
                    ']' -> if (openingChar != openingBracket) return 0
                    ')' -> if (openingChar != openingParenthesis) return 0
                    else -> {
                    }
                }
            }
        }
        if (stack.size != 0) return 0

        return 1
    }

    //FrogRiverOne
    fun solution(X: Int, A: IntArray): Int {
        var steps = X
        val temp = IntArray(steps + 1)

        for (i in A.indices) {
            if (temp[A[i]] == 0) {
                temp[A[i]] = A[i]
                steps--
                if (steps == 0) return i
            }
        }
        return -1
    }

    //Updated
    //Bitwise XOR
    fun solutionOddOccurrencesXOR(A: IntArray): Int {
        var result = 0
        for (x in A) result = result xor x
        return result
    }

    //TapeEquilibrium
    fun solutionTapeEquilibrium(A: IntArray): Int {
        var firstSum = A[0]
        var secondSum = 0

        for (i in 1 until A.size)
            secondSum += A[i]

        var min = abs(firstSum - secondSum)

        for (P in 1 until A.size) {
            if (abs(firstSum - secondSum) < min)
                min = abs(firstSum - secondSum)
            firstSum += A[P]
            secondSum -= A[P]
        }
        return min
    }

    //Updated
    //MissingInteger
    fun solutionMissingInteger(A: IntArray): Int {
        val hSet: MutableSet<Int> = HashSet()
        for (int in A) {
            if (int > 0) {
                hSet.add(int)
            }
        }
        for (i in 1..hSet.size + 1) {
            if (!hSet.contains(i)) {
                return i
            }
        }
        return A.size + 2
    }

    //PermCheck
    fun solutionPermCheck(A: IntArray): Int {
        if (A.size == 1 && A[0] != 1) return 0

        var max = 1

        val hSet: MutableSet<Int> = HashSet()

        for (i in A.indices) {
            if (max < A[i])
                max = A[i]
            hSet.add(A[i])
        }

        if (hSet.size == A.size && max == A.size) return 1

        return 0
    }

    //PassingCars
    fun solutionPassingCars(A: IntArray): Int {
        var count = 0
        var totalOnes = 0
        for (i in A.indices.reversed()) {
            if (A[i] == 1) count++
            else {
                if (totalOnes > 1000000000) {
                    return -1
                }
                totalOnes += count
            }

        }
        return totalOnes
    }

    //Distinct
    fun solution(A: IntArray): Int {
        val hSet: MutableSet<Int> = HashSet()
        for (i in A.indices) {
            if (!hSet.contains(A[i]))
                hSet.add(A[i])
        }
        return hSet.size
    }

    //Nesting
    fun solutionNesting(S: String): Int {
        if (S.length % 2 != 0) return 0

        val openingParenthesis = '('

        val stack = Stack<Char>()

        for (i in S.indices) {
            val char: Char = S[i]
            if (char == openingParenthesis) {
                stack.push(char)
            } else {
                if (stack.isEmpty()) return 0
                if (i == S.length - 1 && stack.size != 1) return 0

                val openingChar = stack.pop()

                when (char) {
                    ')' -> if (openingChar != openingParenthesis) return 0
                    else -> {
                    }
                }
            }
        }
        if (stack.size != 0) return 0

        return 1
    }

}