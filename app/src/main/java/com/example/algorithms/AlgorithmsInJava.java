package com.example.algorithms;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;

public class AlgorithmsInJava {

    //Exactly same code in Kotlin has 88% Total Score
    //OddOccurrences
    public int solutionOddOccurrences(int[] A) {
        if (A.length == 0) return 0;
        if (A.length == 1) return A[0];

        HashSet<Integer> hSet = new HashSet<>();

        for (int value : A) {
            if (hSet.contains(value)) {
                hSet.remove(value);
            } else {
                hSet.add(value);
            }
        }
        for(int i : hSet) {
            return i;
        }
        return 0;
    }

}
